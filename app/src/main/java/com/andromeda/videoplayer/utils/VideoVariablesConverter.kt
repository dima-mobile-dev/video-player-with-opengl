package com.andromeda.videoplayer.utils

object VideoVariablesConverter {

    fun fromSeekbarToOpenGlGamma(gammaInHundreds: Int): Float {
        return 0.5f + gammaInHundreds / 100.0f
    }

    fun fromSeekbarToOpenGlSaturation(saturationInHundreds: Int): Float {
        return 1.0f - saturationInHundreds / 100.0f
    }
}
