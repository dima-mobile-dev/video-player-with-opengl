package com.andromeda.videoplayer.view.touchpad

interface TouchPadEventsListener {
    fun onTouchPadDotPositionUpdate(xAxisPercentage: Int, yAxisPercentage: Int)
}