package com.andromeda.videoplayer.view.touchpad

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import androidx.core.content.ContextCompat
import com.andromeda.videoplayer.R

class TouchPadView : View {

    constructor(context: Context?) : super(context) {
        parseAttributes(null)
    }
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
        parseAttributes(attrs)
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        parseAttributes(attrs)
    }

    var touchPadEventsListener: TouchPadEventsListener? = null

    private var restingPaint = Paint()
    private var activePaint = Paint()

    private var lastTouchPosX = 0f
    private var lastTouchPosY = 0f
    private var isRestingState = true

    private fun parseAttributes(attr: AttributeSet?) {
        val obtainStyledAttributes = context.theme.obtainStyledAttributes(
            attr,
            R.styleable.TouchPadView,
            0,
            0
        )
        obtainStyledAttributes.apply {
            try {
                restingPaint.color = getColor(
                    R.styleable.TouchPadView_resting_color,
                    ContextCompat.getColor(context, R.color.touchpad_resting_color))
                activePaint.color = getColor(
                    R.styleable.TouchPadView_active_color,
                    ContextCompat.getColor(context, R.color.touchpad_active_color))
            } finally {
                recycle()
            }
        }
    }

    public override fun onDraw(canvas: Canvas) {
        if (!isRestingState) {
            canvas.drawLine(0f, lastTouchPosY, measuredWidth.toFloat(), lastTouchPosY, activePaint)
            canvas.drawLine(lastTouchPosX, 0f, lastTouchPosX, measuredHeight.toFloat(), activePaint)
        }
        canvas.drawCircle(lastTouchPosX, lastTouchPosY, 40f, if (isRestingState) restingPaint else activePaint)
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        resetState()
    }

    fun resetState() {
        lastTouchPosX = measuredWidth / 2f
        lastTouchPosY = measuredHeight / 2f
        updateViewAndSendEvents()
    }

    private fun updateViewAndSendEvents() {
        invalidate()
        val xPercentage = lastTouchPosX / measuredWidth.toDouble() * 100
        val yPercentage = lastTouchPosY / measuredHeight.toDouble() * 100

        touchPadEventsListener?.onTouchPadDotPositionUpdate(
            xPercentage.toInt(),
            yPercentage.toInt()
        )
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        val maskedAction = event.actionMasked

        isRestingState = maskedAction == MotionEvent.ACTION_UP
                || maskedAction == MotionEvent.ACTION_POINTER_UP
        if (isRestingState) {
            performClick()
        }
        when (maskedAction) {
            MotionEvent.ACTION_DOWN,
            MotionEvent.ACTION_MOVE,
            MotionEvent.ACTION_UP,
            MotionEvent.ACTION_POINTER_UP,
            MotionEvent.ACTION_POINTER_DOWN -> {
                //Isn't coerceIn just so cool?
                lastTouchPosX = event.x.coerceIn(0f..measuredWidth.toFloat())
                lastTouchPosY = event.y.coerceIn(0f..measuredHeight.toFloat())
            }
        }
        updateViewAndSendEvents()
        return true
    }

    override fun performClick(): Boolean {
        return super.performClick()
    }
}