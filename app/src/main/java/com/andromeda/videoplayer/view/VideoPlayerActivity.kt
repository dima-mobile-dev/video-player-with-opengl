package com.andromeda.videoplayer.view

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.andromeda.videoplayer.R
import com.andromeda.videoplayer.databinding.ActivityMainBinding
import com.andromeda.videoplayer.renderers.VideoPlayerSurfaceRenderer
import com.andromeda.videoplayer.view.touchpad.TouchPadEventsListener
import com.google.android.exoplayer2.ExoPlaybackException
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.Player.STATE_ENDED
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.analytics.AnalyticsListener
import com.google.android.exoplayer2.source.LoadEventInfo
import com.google.android.exoplayer2.source.MediaLoadData
import com.google.android.material.snackbar.Snackbar
import timber.log.Timber
import java.io.IOException

class VideoPlayerActivity : AppCompatActivity(), Player.Listener, TouchPadEventsListener {

    private lateinit var binding: ActivityMainBinding

    private lateinit var exoPlayer: SimpleExoPlayer
    private lateinit var videoPlayerSurfaceRenderer: VideoPlayerSurfaceRenderer

    companion object {
        const val OPEN_VIDEO_REQUEST_CODE = 2
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.touchPadView.touchPadEventsListener = this

        prepareMediaPlayer()

        registerClickListener()
        registerAnalyticsListener()
    }

    private fun prepareMediaPlayer() {
        exoPlayer = SimpleExoPlayer.Builder(this).build()
        exoPlayer.addListener(this)
        videoPlayerSurfaceRenderer = VideoPlayerSurfaceRenderer(binding.surfaceView, exoPlayer)
    }

    private fun registerClickListener() {
        binding.playerWrapper.setOnClickListener {
            if (!playerHasVideo()) {
                return@setOnClickListener
            }
            if (exoPlayer.playbackState == STATE_ENDED) {
                rewindToBeginning()
                exoPlayer.playWhenReady = true
            } else {
                exoPlayer.playWhenReady = !exoPlayer.playWhenReady
            }
        }
    }

    private fun registerAnalyticsListener() {
        exoPlayer.addAnalyticsListener(object : AnalyticsListener {
            override fun onPlayerStateChanged(
                eventTime: AnalyticsListener.EventTime,
                playWhenReady: Boolean,
                playbackState: Int) {
                when (playbackState) {
                    Player.STATE_READY -> {
                        binding.pleaseSelectVideoLabel.visibility = View.GONE
                    }
                }
            }

            override fun onPlayerError(eventTime: AnalyticsListener.EventTime, error: ExoPlaybackException) {
                Timber.e(error, "Error playing video")
                showErrorMessage("Error playing video")
            }

            override fun onLoadError(
                eventTime: AnalyticsListener.EventTime,
                loadEventInfo: LoadEventInfo,
                mediaLoadData: MediaLoadData,
                error: IOException,
                wasCanceled: Boolean
            ) {
                Timber.e(error, "Error loading video")
                showErrorMessage("Error loading video")
            }
        })
    }

    private fun showErrorMessage(errorMessage: String) {
        Snackbar.make(binding.root, errorMessage, Snackbar.LENGTH_LONG)
            .setTextColor(ContextCompat.getColor(this, R.color.white))
            .setBackgroundTint(ContextCompat.getColor(this, R.color.snackbar_error_color))
            .show()
    }

    private fun playerHasVideo() = exoPlayer.mediaItemCount != 0

    private fun loadVideo(uri: Uri) {
        binding.touchPadView.resetState()
        exoPlayer.clearMediaItems()
        exoPlayer.addMediaItem(MediaItem.fromUri(uri))
        exoPlayer.prepare()
        rewindToBeginning()
        exoPlayer.playWhenReady = true
    }


    private fun rewindToBeginning() {
        exoPlayer.seekTo(0)
    }

    override fun onVideoSizeChanged(
        width: Int, height: Int, unappliedRotationDegrees: Int,
        pixelWidthHeightRatio: Float) {
        videoPlayerSurfaceRenderer.updateCurrentVideoDimens(width, height)
    }

    override fun onSurfaceSizeChanged(width: Int, height: Int) {
        //no-op
    }

    override fun onRenderedFirstFrame() {
        //no-op
    }

    override fun onTouchPadDotPositionUpdate(xAxisPercentage: Int, yAxisPercentage: Int) {
        videoPlayerSurfaceRenderer.updateVideoVisuals(
            brightnessPercentage = xAxisPercentage,
            saturationPercentage = yAxisPercentage)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.actionbar_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    // handle button activities
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id: Int = item.itemId
        if (id == R.id.action_pick_video) {
            pickVideo()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun pickVideo() {
        val intent = Intent(Intent.ACTION_OPEN_DOCUMENT)
        intent.addCategory(Intent.CATEGORY_OPENABLE)
        intent.type = "video/*"
        startActivityForResult(intent, OPEN_VIDEO_REQUEST_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, resultData: Intent?) {
        super.onActivityResult(requestCode, resultCode, resultData)
        if (requestCode == OPEN_VIDEO_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                if (resultData?.data != null) {
                    loadVideo(resultData.data!!)
                } else {
                    showErrorMessage("File uri not found")
                }
            } else {
                showErrorMessage("User cancelled file browsing")
            }
        }
    }

    override fun onPause() {
        super.onPause()
        exoPlayer.playWhenReady = false
    }
}
