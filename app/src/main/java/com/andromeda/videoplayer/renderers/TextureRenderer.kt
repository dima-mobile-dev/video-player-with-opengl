package com.andromeda.videoplayer.renderers

import android.graphics.SurfaceTexture
import android.opengl.GLES11Ext
import android.opengl.GLES20
import android.opengl.Matrix
import java.nio.ByteBuffer
import java.nio.ByteOrder
import java.nio.FloatBuffer
import java.nio.IntBuffer

class TextureRenderer: AbstractRenderer() {

    private val vertexShaderCode =
            """
                precision highp float;
                attribute vec3 vertexPosition;
                attribute vec2 uvs;
                varying vec2 varUvs;
                uniform mat4 texMatrix;
                uniform mat4 mvp;
              
                void main() {
                    varUvs = (texMatrix * vec4(uvs.x, uvs.y, 0, 1.0)).xy;
                    gl_Position = mvp * vec4(vertexPosition, 1.0);
                }
        """

    private val fragmentShaderCode =
        """
            #extension GL_OES_EGL_image_external : require
            precision mediump float;
    
            varying vec2 varUvs;
            uniform samplerExternalOES texSampler;
            uniform float brightness;
            uniform float saturation;
            
            const vec3 W = vec3(0.2125, 0.7154, 0.0721);
            
            vec4 calculateSaturationVector() {
                vec3 irgb = texture2D(texSampler, varUvs).rgb; 
                float luminance = dot(irgb, W);
                vec3 target = vec3(luminance, luminance, luminance); 
                lowp vec4 saturationVector = vec4(mix(target, irgb, saturation), 1.);
                return saturationVector;
            }
            
            void main() {
                lowp vec4 brightnessVector = texture2D(texSampler, varUvs) * brightness;
                gl_FragColor = brightnessVector * calculateSaturationVector();
            }
 
        """

    private var vertices = floatArrayOf(
            -1.0f, -1.0f, 0.0f, 0f, 0f,
            -1.0f, 1.0f, 0.0f, 0f, 1f,
            1.0f, 1.0f, 0.0f, 1f, 1f,
            1.0f, -1.0f, 0.0f, 1f, 0f
    )

    private var indices = intArrayOf(
            2, 1, 0, 0, 3, 2
    )

    private var vertexBuffer: FloatBuffer = ByteBuffer.allocateDirect(vertices.size * 4).run {
        order(ByteOrder.nativeOrder())
        asFloatBuffer().apply {
            put(vertices)
            position(0)
        }
    }

    private var indexBuffer: IntBuffer = ByteBuffer.allocateDirect(indices.size * 4).run {
        order(ByteOrder.nativeOrder())
        asIntBuffer().apply {
            put(indices)
            position(0)
        }
    }

    private var program: Int = 0

    private var vertexHandle: Int = 0

    private var bufferHandles = IntArray(2)
    private var uvsHandle: Int = 0
    private var texMatrixHandle: Int = 0
    private var mvpHandle: Int = 0
    private var samplerHandle: Int = 0
    private var brightnessHandle: Int = 0
    private var saturationHandle: Int = 0

    private val textureHandles = IntArray(1)

    val texId: Int
        get() = textureHandles[0]

    private val mvpMatrix = FloatArray(16)
    private val texMatrix = FloatArray(16)

    private var brightness = 1f
    private var saturation = 0.5f

    private var videoWidth = 0
    private var videoHeight = 0

    fun surfaceCreated() {
        val vertexShader: Int = loadShader(GLES20.GL_VERTEX_SHADER, vertexShaderCode)
        val fragmentShader: Int = loadShader(GLES20.GL_FRAGMENT_SHADER, fragmentShaderCode)

        program = GLES20.glCreateProgram().also {
            GLES20.glAttachShader(it, vertexShader)
            GLES20.glAttachShader(it, fragmentShader)
            GLES20.glLinkProgram(it)

            vertexHandle = GLES20.glGetAttribLocation(it, "vertexPosition")
            uvsHandle = GLES20.glGetAttribLocation(it, "uvs")
            texMatrixHandle = GLES20.glGetUniformLocation(it, "texMatrix")
            mvpHandle = GLES20.glGetUniformLocation(it, "mvp")
            samplerHandle = GLES20.glGetUniformLocation(it, "texSampler")
            brightnessHandle = GLES20.glGetUniformLocation(it, "brightness")
            saturationHandle = GLES20.glGetUniformLocation(it, "saturation")
        }

        GLES20.glGenBuffers(2, bufferHandles, 0)

        GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, bufferHandles[0])
        GLES20.glBufferData(GLES20.GL_ARRAY_BUFFER, vertices.size * 4, vertexBuffer, GLES20.GL_DYNAMIC_DRAW)

        GLES20.glBindBuffer(GLES20.GL_ELEMENT_ARRAY_BUFFER, bufferHandles[1])
        GLES20.glBufferData(GLES20.GL_ELEMENT_ARRAY_BUFFER, indices.size * 4, indexBuffer, GLES20.GL_DYNAMIC_DRAW)

        val target = GLES11Ext.GL_TEXTURE_EXTERNAL_OES
        GLES20.glGenTextures(1, textureHandles, 0)
        GLES20.glBindTexture(target, textureHandles[0])

        GLES20.glTexParameteri(target, GLES20.GL_TEXTURE_MIN_FILTER,
                GLES20.GL_NEAREST)
        GLES20.glTexParameteri(target, GLES20.GL_TEXTURE_MAG_FILTER,
                GLES20.GL_NEAREST)
        GLES20.glTexParameteri(target, GLES20.GL_TEXTURE_WRAP_S,
                GLES20.GL_CLAMP_TO_EDGE)
        GLES20.glTexParameteri(target, GLES20.GL_TEXTURE_WRAP_T,
                GLES20.GL_CLAMP_TO_EDGE)

        GLES20.glEnable(GLES20.GL_BLEND)
        GLES20.glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA)
    }


    private fun loadShader(type: Int, shaderCode: String): Int {
        return GLES20.glCreateShader(type).also { shader ->
            GLES20.glShaderSource(shader, shaderCode)
            GLES20.glCompileShader(shader)
        }
    }

    fun drawTexture(surfaceTexture: SurfaceTexture) {
        surfaceTexture.getTransformMatrix(texMatrix)

        GLES20.glUseProgram(program)

        GLES20.glUniformMatrix4fv(texMatrixHandle, 1, false, texMatrix, 0)
        GLES20.glUniformMatrix4fv(mvpHandle, 1, false, mvpMatrix, 0)

        GLES20.glUniform1f(brightnessHandle, brightness)
        GLES20.glUniform1f(saturationHandle, saturation)

        GLES20.glActiveTexture(GLES20.GL_TEXTURE0)

        val target = GLES11Ext.GL_TEXTURE_EXTERNAL_OES
        GLES20.glBindTexture(target, textureHandles[0])

        GLES20.glTexParameteri(target, GLES20.GL_TEXTURE_MIN_FILTER,
                GLES20.GL_NEAREST)
        GLES20.glTexParameteri(target, GLES20.GL_TEXTURE_MAG_FILTER,
                GLES20.GL_LINEAR)
        GLES20.glTexParameteri(target, GLES20.GL_TEXTURE_WRAP_S,
                GLES20.GL_CLAMP_TO_EDGE)
        GLES20.glTexParameteri(target, GLES20.GL_TEXTURE_WRAP_T,
                GLES20.GL_CLAMP_TO_EDGE)

        GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, bufferHandles[0])
        GLES20.glBindBuffer(GLES20.GL_ELEMENT_ARRAY_BUFFER, bufferHandles[1])

        GLES20.glEnableVertexAttribArray(vertexHandle)

        GLES20.glVertexAttribPointer(vertexHandle, 3, GLES20.GL_FLOAT, false, 4 * 5, 0)
        GLES20.glEnableVertexAttribArray(uvsHandle)
        GLES20.glVertexAttribPointer(uvsHandle, 2, GLES20.GL_FLOAT, false, 4 * 5, 3 * 4)

        GLES20.glDrawElements(GLES20.GL_TRIANGLES, indices.size, GLES20.GL_UNSIGNED_INT, 0)
    }

    override fun generateMvps() {
        Matrix.setIdentityM(mvpMatrix, 0)

        val videoAspectRatio = videoWidth / videoHeight.toFloat()
        val contentFrameAspectRatio = contentFrameWidth / contentFrameHeight.toFloat()

        var videoToContentFrameScaleX = 1.0f
        var videoToContentFrameScaleY = 1.0f
        if (videoAspectRatio > contentFrameAspectRatio) {
            videoToContentFrameScaleY = contentFrameAspectRatio / videoAspectRatio
        } else {
            videoToContentFrameScaleX = videoAspectRatio / contentFrameAspectRatio
        }

        Matrix.scaleM(mvpMatrix, 0, videoToContentFrameScaleX, videoToContentFrameScaleY, 1f)
    }

    fun updateCurrentVideoDimens(vWidth: Int, vHeight: Int) {
        videoWidth = vWidth
        videoHeight = vHeight
        generateMvps()
    }

    fun updateVideoVisuals(openGlSaturation: Float, openGlGamma: Float) {
        saturation = openGlSaturation
        brightness = openGlGamma
    }
}