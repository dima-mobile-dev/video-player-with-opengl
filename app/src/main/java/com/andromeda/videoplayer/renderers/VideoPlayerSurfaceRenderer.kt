package com.andromeda.videoplayer.renderers

import android.graphics.SurfaceTexture
import android.graphics.SurfaceTexture.OnFrameAvailableListener
import android.opengl.GLES20
import android.opengl.GLSurfaceView
import android.os.Handler
import android.view.Surface
import com.andromeda.videoplayer.utils.VideoVariablesConverter
import com.google.android.exoplayer2.SimpleExoPlayer
import javax.microedition.khronos.egl.EGLConfig
import javax.microedition.khronos.opengles.GL10

class VideoPlayerSurfaceRenderer constructor(
    surfaceView: GLSurfaceView,
    private val simpleExoPlayer: SimpleExoPlayer,
) : GLSurfaceView.Renderer, OnFrameAvailableListener {

    private val applicationHandler = Handler(simpleExoPlayer.applicationLooper)

    private val textureRender: TextureRenderer = TextureRenderer()
    private lateinit var surfaceTexture: SurfaceTexture

    private var shouldUpdateSurfaceTexture = false

    init {
        surfaceView.setEGLContextClientVersion(2)
        surfaceView.setRenderer(this)
        surfaceView.renderMode = GLSurfaceView.RENDERMODE_CONTINUOUSLY
    }

    override fun onSurfaceCreated(gl: GL10, config: EGLConfig) {
        textureRender.surfaceCreated()

        surfaceTexture = SurfaceTexture(textureRender.texId)
        surfaceTexture.setOnFrameAvailableListener(this)

        applicationHandler.post {
            simpleExoPlayer.setVideoSurface(Surface(surfaceTexture))
        }

        synchronized(this) { shouldUpdateSurfaceTexture = false }
    }

    override fun onDrawFrame(glUnused: GL10?) {
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT)

        synchronized(this) {
            if (shouldUpdateSurfaceTexture) {
                surfaceTexture.updateTexImage()
                shouldUpdateSurfaceTexture = false
            }
        }
        textureRender.drawTexture(surfaceTexture)
    }

    override fun onSurfaceChanged(glUnused: GL10, width: Int, height: Int) {
        textureRender.setSurfaceDimens(width, height)
        GLES20.glViewport(0, 0, width, height)
    }

    @Synchronized
    override fun onFrameAvailable(surface: SurfaceTexture) {
        shouldUpdateSurfaceTexture = true
    }

    fun updateVideoVisuals(saturationPercentage: Int, brightnessPercentage: Int) {
        val openGlGamma = VideoVariablesConverter.fromSeekbarToOpenGlGamma(brightnessPercentage)
        val openGlSaturation = VideoVariablesConverter.fromSeekbarToOpenGlSaturation(saturationPercentage)
        textureRender.updateVideoVisuals(openGlSaturation, openGlGamma)
    }

    fun updateCurrentVideoDimens(width: Int, height: Int) {
        textureRender.updateCurrentVideoDimens(width, height)
    }
}