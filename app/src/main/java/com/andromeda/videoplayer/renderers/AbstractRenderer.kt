package com.andromeda.videoplayer.renderers


abstract class AbstractRenderer {

    protected var contentFrameWidth = 0
    protected var contentFrameHeight = 0

    fun setSurfaceDimens(frameWidth: Int, frameHeight: Int) {
        contentFrameWidth = frameWidth
        contentFrameHeight = frameHeight

        generateMvps()
    }

    abstract fun generateMvps()

}